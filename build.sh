#!/usr/bin/env bash
Tags=(
    18.04
    20.04
    21.04
)


function help(){
    echo "nginx docker bash"
    echo
    echo "Usage:"
    echo "  $0 [flags]"
    echo "  $0 [command]"
    echo
    echo "Available Commands:"
    echo "  help              help for $0"
    echo "  tags              display tag list"
    echo "  apply             apply template to Dockerfile"
    echo "  build             build Dockerfile"
    echo "  push              push to docker hub"
    echo
    echo "Flags:"
    echo "  -h, --help          help for $0"
}
function tags(){
    for tag in ${Tags[@]}
    do
        echo $tag
    done
}
function applyGit(){
    local from="$1"
    local dir="$2"
    local tag="$from-git"
    echo apply $tag
    mkdir "$dir/docker/$tag" -p
    echo "FROM king011/ubuntu:$from" > "$dir/docker/$tag/Dockerfile"
    echo 'RUN set -eux; \' >> "$dir/docker/$tag/Dockerfile"
    echo '	apt-get update; \' >> "$dir/docker/$tag/Dockerfile"
    echo '	apt-get install -y --no-install-recommends git; \' >> "$dir/docker/$tag/Dockerfile"
    echo '	rm -rf /var/lib/apt/lists/*' >> "$dir/docker/$tag/Dockerfile"
}
function buildTag(){
    local tag="$1"
    local dir="$2"
    echo build $tag
    cd "$dir/docker/$tag" 
    sudo docker build -t "king011/ubuntu:$tag" .
}
function pushTag(){
    local tag="$1"
    echo push $tag
    sudo docker push "king011/ubuntu:$tag"
}
function apply(){
    local dir=$(cd $(dirname $BASH_SOURCE) && pwd)
    set -eu;
    for tag in ${Tags[@]}
    do
        echo apply $tag

        mkdir "$dir/docker/$tag" -p

        echo "FROM ubuntu:$tag" > "$dir/docker/$tag/Dockerfile"
        cat "$dir/Dockerfile" >> "$dir/docker/$tag/Dockerfile"

        cp "$dir/docker-entrypoint.sh" "$dir/docker/$tag/docker-entrypoint.sh"
        chmod +x "$dir/docker/$tag/docker-entrypoint.sh"

        applyGit "$tag" "$dir"
    done
}
function build(){
    local dir=$(cd $(dirname $BASH_SOURCE) && pwd)
    set -eu;
    for tag in ${Tags[@]}
    do
        buildTag "$tag" "$dir"
        buildTag "$tag-git" "$dir"
    done
}
function push(){
    set -eu;
    for tag in ${Tags[@]}
    do
        pushTag "$tag"
        pushTag "$tag-git"
    done
}
case "$1" in
    help|-h|--help)
        help
    ;;
    tags)
        tags
    ;;
    apply)
        apply
    ;;
    build)
        build
    ;;
    push)
        push
    ;;
    *)
        help
        exit 1
    ;;
esac