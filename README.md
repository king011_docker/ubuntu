# ubuntu

docker ubuntu images

Follow ubuntu tls, and install gosu and scripts to automatically create users and run programs with this user.

# tags

* 18.04
* 20.04
* 21.04
* xxx-git

# Environment variable

* EXEC_USER -> If it is not empty, create this user and run the command with this user.
* EXEC_USER_ID -> If it is not 0, set it as the user id.
* EXEC_USER_SYSTEM -> If non-zero, create a system user.
* EXEC_USER_SHELL -> If non-empty is used to set the user shell.
* EXEC_USER_HOME -> If it is not empty, it is used to set the user's home directory.
* EXEC_USER_CREATE_HOME -> If non-zero, create a home directory for the user.
* EXEC_GROUP -> If it is not empty, create a user group for the user.
* EXEC_GROUP_ID -> If it is not 0, set it as the group id.
* EXEC_GROUP_SYSTEM -> If non-zero, create a system group.

The following example automatically creates a user named king and runs bash under this user
```
docker run --rm -e EXEC_USER=king  -it king011/ubuntu:21.04 bash
```