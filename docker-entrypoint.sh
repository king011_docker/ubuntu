#!/bin/bash

set -eu;

function group_user_add(){
    local group_opts=()
    local user_opts=()
    # group opts
    if [[ "$EXEC_USER" == "" ]];then
        return 0
    fi
    if [[ "$EXEC_GROUP" == "" ]];then
        EXEC_GROUP="$EXEC_USER"
    fi
    # user opts
    if [[ "$EXEC_GROUP_ID" != 0 ]];then
        group_opts=("${group_opts[@]}" "-g" "$EXEC_GROUP_ID")
    fi
    if [[ "$EXEC_GROUP_SYSTEM" != 0 ]];then
        group_opts=("${group_opts[@]}" "-r")
    fi
    group_opts=("${group_opts[@]}" "$EXEC_GROUP")

    if [[ "$EXEC_USER_ID" != 0 ]];then
        user_opts=("${user_opts[@]}" "-u" "$EXEC_USER_ID")
    fi
    if [[ "$EXEC_USER_SYSTEM" != 0 ]];then
        user_opts=("${user_opts[@]}" "-r")
    fi
    if [[ "$EXEC_USER_SHELL" != "" ]];then
        user_opts=("${user_opts[@]}" "-s" "$EXEC_USER_SHELL")
    fi
    if [[ "$EXEC_USER_HOME" != "" ]];then
        user_opts=("${user_opts[@]}" "-d" "$EXEC_USER_HOME")
    fi
    if [[ "$EXEC_USER_CREATE_HOME" == 0 ]];then
        user_opts=("${user_opts[@]}" "-M")
    else
        user_opts=("${user_opts[@]}" "-m")
    fi

    # create group and user
    user_opts=("${user_opts[@]}" "-g" "$EXEC_GROUP")
    user_opts=("${user_opts[@]}" "$EXEC_USER")

	set +e;
    egrep "^$EXEC_GROUP:" /etc/group
    if [ $? -ne 0 ]
    then
        groupadd ${group_opts[@]}
    fi
    
    egrep "^$EXEC_USER:" /etc/passwd
    if [ $? -ne 0 ]
    then
        useradd ${user_opts[@]}
    fi
	set -e;
}
function group_user_clear(){
    unset EXEC_USER
	unset EXEC_USER_ID
	unset EXEC_USER_SYSTEM
    unset EXEC_USER_SHELL
    unset EXEC_USER_HOME
    unset EXEC_USER_CREATE_HOME
	unset EXEC_GROUP
	unset EXEC_GROUP_ID
	unset EXEC_GROUP_SYSTEM
}
group_user_add

if [[ "$EXEC_USER" == "" ]];then
    exec "$@"
else
    user=$EXEC_USER
    group_user_clear
    exec gosu "$user" "$@"
fi
